package com.company;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Server {
    private static Socket clientSocket;
    private static ServerSocket server;
    private static BufferedReader in;
    private static BufferedWriter out;

    public static void main(String[] args) {
        try {
            try {
                server = new ServerSocket(54321);
                System.out.println("Сервер запущен!");
                clientSocket = server.accept();
                try {
                    in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                    out = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));

                    String products[] = {"Молоко","Конфетки","Хлеб", "Гречка", "Яблоки", "Апельсины"};
                    Pattern getProductsPattern = Pattern.compile("get_products");
                    Pattern getProductByIdPattern = Pattern.compile("get_product [0-5]");
                    Matcher matcher;

                    String clientInput = in.readLine();
                    System.out.println(clientInput);

                    matcher = getProductsPattern.matcher(clientInput);
                    int min = 20;
                    int max = 100;
                    int diff = max - min;
                    Random random = new Random();

                    String response = "";
                    if(matcher.matches()){
                        for (int i = 0; i < products.length; i++) {
                            int j = random.nextInt(diff + 1);
                            j += min;
                            response += i + " - " + products[i] + " - " + j + "грн " + "|";
                        }
                        response += "\n";
                        out.write(response);
                        out.flush();
                    }

                    String clientInput2 = in.readLine();
                    matcher = getProductByIdPattern.matcher(clientInput2);
                    if(matcher.matches()){
                        Integer number = Integer.parseInt(Character.toString(clientInput2.charAt(clientInput2.length()-1)));
                        out.write("Получите товар: " + products[number] + "\n");
                        out.flush();
                    }

                } finally {
                    System.out.println("Пока!");
                    clientSocket.close();
                    in.close();
                    out.close();
                }
            } finally {
                System.out.println("Сервер закрыт!");
                server.close();
            }
        } catch (IOException e) {
            System.err.println(e);
        }
    }
}
