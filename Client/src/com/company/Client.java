package com.company;
import java.io.*;
import java.net.Socket;

public class Client {

    private static Socket clientSocket;
    private static BufferedReader reader;
    private static BufferedReader in;
    private static BufferedWriter out;

    public static void main(String[] args) {
        try {
            try {
                clientSocket = new Socket("localhost", 54321);
                reader = new BufferedReader(new InputStreamReader(System.in));
                in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                out = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));

                System.out.println("Введите команду get_products");
                String clientInput = reader.readLine();
                out.write(clientInput + "\n");
                out.flush();
                String serverResponse = in.readLine();
                System.out.println("Возвращаемое значение: ");
                for (String retval : serverResponse.split("\\|")) {
                    System.out.println(retval);
                }

                System.out.println("Введите команду get_product <id>:");
                String clientInput2 = reader.readLine();
                out.write(clientInput2 + "\n");
                out.flush();
                String serverResponse2 = in.readLine();
                System.out.println(serverResponse2);
            } finally {
                System.out.println("Клиент завершает работу...");
                clientSocket.close();
                in.close();
                out.close();
            }
        } catch (IOException e) {
            System.err.println(e);
        }
    }
}
